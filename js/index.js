$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $(".carousel").carousel({
        interval: 3000,
    });
    $("#contactoBtn").on('show.bs.modal', function (e) {
        console.log('el modal se esta mostrando');
        $("#contacto").removeClass("btn");

        $("#contacto").prop('disabled', true)
    });

    $("#contactoBtn").on('shown.bs.modal', function (e) {
        console.log('el modal se mostro');
    });

    $("#contactoBtn").on('hide.bs.modal', function (e) {
        console.log('el modal se esta cerrando');
    });

    $("#contactoBtn").on('hidden.bs.modal', function (e) {
        console.log('el modal se cerro');
        // setTimeout(() => { console.log("World!"); }, 5000);
        $("#contacto").addClass("btn btn-primary");
        // setTimeout(() => { console.log("World!"); }, 3000);
        $("#contacto").prop('disabled', false)
    });

});
